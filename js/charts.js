var history365jsonURL = 'http://coincap.io/history/365day/'; //add symbol
var historyjsonURL = 'http://coincap.io/history/'; //add symbol
var coinsURL_cmc= 'https://files.coinmarketcap.com/generated/search/quick_search.json';
var coinsURL = 'http://coincap.io/coins/';
var COINS_RANGE = 300;

// set display options
var options = {
  showLink: false,
  displayModeBar: false
};

var selectorOptions = {
    buttons: [{
        step: 'day',
        stepmode: 'backward',
        count: 1,
        label: '1d'
    },{
        step: 'day',
        stepmode: 'backward',
        count: 7,
        label: '7d'
    },{
        step: 'month',
        stepmode: 'backward',
        count: 1,
        label: '1m'
    }, {
        step: 'month',
        stepmode: 'backward',
        count: 6,
        label: '6m'
    }, {
        step: 'year',
        stepmode: 'todate',
        count: 1,
        label: 'YTD'
    }, {
        step: 'year',
        stepmode: 'backward',
        count: 1,
        label: '1y'
    }, {
        step: 'all',
    }],
};

function generate_plot(coin_info) {
    Plotly.d3.select('#graphs').append('div').attr('id','graph-'+coin_info['symbol']);
    Plotly.d3.json(historyjsonURL+coin_info['symbol'], function(err, jsonData) {
      if(err) throw err;
      let graph_data = prepDataJson(jsonData);
        let layout = {
            title: `${coin_info['name']} [${coin_info['rank']}] - ${coin_info['symbol']}`,
            xaxis: {
                rangeselector: selectorOptions,
                rangeslider: {},
                autorange: true
            },
            yaxis: {
                autorange: true,
                fixedrange: false
            },
        };
        Plotly.plot('graph-'+coin_info['symbol'], graph_data, layout, options);
    });
}


function prepDataJson(jsonData) {
    let x = [];
    let y = [];

    jsonData['price'].forEach(function(datum, i) {

        x.push(new Date(datum[0]));
        y.push(datum[1]);
    });

    return [{
        type: 'scatter',
        x: x,
        y: y
    }];
}


Plotly.d3.json(coinsURL_cmc, function(err, jsonData) {
  if(err) throw err;
  for (let i = 0; i < COINS_RANGE; i++) {
        generate_plot(jsonData[i]);
    }
});